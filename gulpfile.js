// gulp concat

var gulp         = require('gulp');
var concat       = require('gulp-concat');
var uglify       = require('gulp-uglify');
var clean        = require('gulp-clean');
var sass         = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');

gulp.task('watchSass', function() {
    gulp.watch('./public/sass/**/*.scss', ['styles']);
});

gulp.task('styles', function() {

    gulp.src('./public/sass/**/*.scss')
        .pipe(autoprefixer({
            browsers: ['last 3 versions']
        }))
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./public/css/'));
});


gulp.task('autoprefix', function() {

    return gulp.src('./public/css/**/*.css')
        .pipe(autoprefixer({
            browsers: ['last 3 versions']
        }))
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./public/css/'));
});
