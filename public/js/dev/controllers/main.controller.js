'use strict';

angular.module('bandsInTownApp')

.controller('MainController', ['$mdDialog', 'data',
    function($mdDialog, data) {

    var vm = this;

    vm.data = data;
    vm.search = {
        text: ''
    };
    vm.clearSearchInput = clearSearchInput;
    vm.selectArtist = selectArtist;
    vm.moreEventInfo = moreEventInfo;


    function clearSearchInput(){
        vm.search.text = '';
        data.noResult = false;
        data.autoCompleteData = [];
        data.events = [];
    }

    function selectArtist(artist){
        vm.search.text = artist.name;
        data.selectedArtist = artist.name;
        data.autoCompleteData = [];
    }

     function moreEventInfo(ev, event) {
        $mdDialog.show({
            locals:{event: event},
            controller: DialogController,
            controllerAs: 'ctrl',
            templateUrl: 'public/views/dialog.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose:true
        })}

    function DialogController($mdDialog, event) {

        var vm = this;

        vm.event = event;

        vm.getDate = function (date) {
            return moment(date).format('DD-MM-YYYY');
        };

        vm.cancel = function() {
            $mdDialog.cancel();
        };
    }

}]);
