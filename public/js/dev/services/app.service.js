'use strict';

angular.module('bandsInTownApp')

.constant('autoCompleteUrl', 'https://cors.io/?https://www.bandsintown.com/searchSuggestions')
.constant('dataUrl', 'https://cors.io/?https://rest.bandsintown.com/artists/')
.constant('apiKey', '9469c3d9497e7b77ba6a17d06adc2ef7')

.factory('data', ['$http', '$timeout', '$filter', '$mdDialog', 'autoCompleteUrl', 'dataUrl', 'apiKey',
    function ($http, $timeout, $filter, $mdDialog, autoCompleteUrl, dataUrl, apiKey) {

        var factory = {};

        factory.selectedArtist = '';
        factory.fromDate = new Date();
        factory.toDate = new Date();
        factory.noResult = false;

        factory.fetchAutoComplete = function (searchText) {

            var replacedSearchText = $filter('specialChar')(searchText);

            $http.get(autoCompleteUrl + '?searchTerm=' + replacedSearchText).then(function successCallback(response) {

                $timeout(function () {
                    if (searchText.length > 0) {

                        factory.autoCompleteData = response.data.artists;
                    }
                    else {

                        factory.autoCompleteData = [];
                    }
                }, 0);

            }, function errorCallback(error) {
                console.log(error);
            });

        };

        factory.searchEvent = function (ev) {

            if (factory.selectedArtist.length !== 0) {

                var replacedName = $filter('specialChar')(factory.selectedArtist);
                var fromDate = moment(factory.fromDate).format('YYYY-MM-DD');
                var toDate = moment(factory.toDate).format('YYYY-MM-DD');

                var url = dataUrl + replacedName.toLowerCase() + '/events?app_id=' + apiKey + '&date=' + fromDate + '%2C' + toDate;

                factory.noResult = false;

                $http.get(url).then(function successCallback(response){

                    if(response.data.length > 0){

                        factory.events = response.data;
                    }
                    else {

                        factory.noResult = true;
                    }

                })
            }
            else {

                $mdDialog.show(
                    $mdDialog.alert()
                        .parent(angular.element(document.querySelector('#popupContainer')))
                        .clickOutsideToClose(true)
                        .title('Błąd')
                        .textContent('Proszę podać nazwę artysty.')
                        .ok('Ok')
                        .targetEvent(ev)
                );
            }

        };

        return factory;

    }]);


