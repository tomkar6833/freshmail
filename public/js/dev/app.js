'use strict';

angular.module('bandsInTownApp', ['ngMaterial'])

.config(['$mdDateLocaleProvider', '$locationProvider', function($mdDateLocaleProvider, $locationProvider) {

    $mdDateLocaleProvider.formatDate = function(date) {
        return moment(date).format('DD-MM-YYYY');
    };

    if (window.history && window.history.pushState) {

        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
    }

}]);


