'use strict';

angular.module('bandsInTownApp')
.filter('specialChar', function () {
    return function (word) {

        var wordTable = word.split('');
        var newWordTable = [];

        wordTable.map(function(item){

            switch(item){
                case '/':
                    newWordTable.push('%252F');

                    break;

                case '?':
                    newWordTable.push('%253F');

                    break;

                case '*':
                    newWordTable.push('%252A');

                    break;

                case '"':
                    newWordTable.push('%27C');

                    break;

                case ' ':
                    newWordTable.push('+');

                    break;

                default:
                    newWordTable.push(item);
            }
        });

        return newWordTable.join('');
    };
});